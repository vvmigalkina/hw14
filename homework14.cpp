﻿#include <iostream>
#include <string>

int main()
{
	std::string variable1 = "Variable1";

	std::cout << variable1 << "\n";
	std::cout << "Variable length:" << variable1.length() << "\n";
	std::cout << "First symbol:" << variable1[0] << "\n";
	std::cout << "Last symbol:" << variable1[8] << "\n";

	return 0;
}